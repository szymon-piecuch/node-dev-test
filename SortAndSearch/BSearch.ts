export class BSearch {
  private static instance?: BSearch;
  private _operations = 0;

  private constructor() {}

  static getInstance(): BSearch {
    if (!BSearch.instance) {
      BSearch.instance = new BSearch();
    }

    return BSearch.instance;
  }

  public search(values: number[], target: number): number {
    this._operations++;

    let left = 0;
    let right = values.length - 1;
    let middle: number;

    while (left <= right) {
      middle = Math.floor((left + right) / 2);

      if (values[middle] === target) {
        return middle;
      }

      if (values[middle] < target) {
        left = middle + 1;
      } else {
        right = middle - 1;
      }
    }

    return -1;
  }

  get operations() {
    return this._operations;
  }
}
