// Bubble sort - O(n^2)
export class BSort {
  private values: number[] = [];

  constructor(originalValues: number[]) {
    this.values = [...originalValues];
    let swapped: boolean;

    do {
      swapped = false;
      for (let i = 0; i < this.values.length; i++) {
        if (this.values[i] > this.values[i + 1]) {
          [this.values[i], this.values[i + 1]] = [this.values[i + 1], this.values[i]];
          swapped = true;
        }
      }
    } while (swapped);
  }

  public getValues(): Number[] {
    return this.values;
  }
}
