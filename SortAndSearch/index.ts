import { ASort } from "./ASort";
import { BSort } from "./BSort";
import { BSearch } from "./BSearch";

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

const asorted = new ASort(unsorted);
const bsorted = new BSort(unsorted);

console.log(unsorted); // to be sure, that values haven't been modified
console.log(asorted.getValues());
console.log(bsorted.getValues());

const bSearch = BSearch.getInstance();
console.log(bSearch.search(elementsToFind, 13)); // should be 2
console.log(bSearch.search(elementsToFind, 500)); // should be -1
console.log(bSearch.operations); // should be 2
