// Selection sort - O(n^2)
export class ASort {
  private values: number[];

  constructor(originalValues: number[]) {
    this.values = [...originalValues];
    let minimal: number;

    for (let i = 0; i < this.values.length; i++) {
      minimal = i;

      for (let j = i + 1; j < this.values.length; j++) {
        if (this.values[j] < this.values[minimal]) {
          minimal = j;
        }
      }

      [this.values[i], this.values[minimal]] = [this.values[minimal], this.values[i]];
    }
  }

  public getValues(): number[] {
    return this.values;
  }
}
