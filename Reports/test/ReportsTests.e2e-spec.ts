import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';

describe('', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(404);
  });

  it('Fetches product report for 07.07.2019', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-07')
      .expect(200)
      .expect([
        { quantity: 2, productName: 'Black sport shoes', totalPrice: 220 },
        { quantity: 1, productName: 'Cotton t-shirt XL', totalPrice: 25.75 },
      ]);
  });

  it('Returnes empty array when no best sellers', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-01-01')
      .expect(200)
      .expect([]);
  });

  it('Returnes bad request when date is invalid for product report', () => {
    return request(app.getHttpServer())
      .get('/report/products/not-valid-date')
      .expect(400)
      .expect({
        statusCode: 400,
        error: 'Bad Request',
        message: 'Incorrect date: not-valid-date',
      });
  });

  it('Fetches customer report for 07.07.2019', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-07')
      .expect(200)
      .expect([
        { customerName: 'John Doe', totalPrice: 135.75 },
        { customerName: 'Jane Doe', totalPrice: 110 },
      ]);
  });

  it('Returnes empty array when no best buyers', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-01-01')
      .expect(200)
      .expect([]);
  });

  it('Returnes bad request when date is invalid for customer report', () => {
    return request(app.getHttpServer())
      .get('/report/customer/not-valid-date')
      .expect(400)
      .expect({
        statusCode: 400,
        error: 'Bad Request',
        message: 'Incorrect date: not-valid-date',
      });
  });
});
