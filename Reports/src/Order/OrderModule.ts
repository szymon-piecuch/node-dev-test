import { Module } from '@nestjs/common';
import { Repository } from './Service/Repository';
import { OrderMapper } from './Service/OrderMapper';
import { CustomerMapper } from './Service/CustomerMapper';
import { ProductMapper } from './Service/ProductMapper';

@Module({
  controllers: [],
  providers: [Repository, OrderMapper, CustomerMapper, ProductMapper],
  exports: [OrderMapper],
})
export class OrderModule {}
