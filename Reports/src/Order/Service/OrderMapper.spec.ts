import { Test } from '@nestjs/testing';
import { OrderMapper } from './OrderMapper';
import { Repository } from './Repository';
import { CustomerMapper } from './CustomerMapper';
import { ProductMapper } from './ProductMapper';
import { Customer } from '../Model/Customer';
import { Product } from '../Model/Product';

describe('OrderMapper', () => {
  let orderMapper: OrderMapper;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [Repository, OrderMapper, CustomerMapper, ProductMapper],
    }).compile();

    orderMapper = module.get<OrderMapper>(OrderMapper);
  });

  it('Returns existing orders', async () => {
    const orders = await orderMapper.getOrdersForDay(new Date('2019-08-08'));

    expect(orders.length).toBe(3);
    expect(orders[0].customer instanceof Customer).toBeTruthy();
    expect(orders[0].products.length).toBe(1);
    expect(orders[0].products[0] instanceof Product).toBeTruthy();
  });
});
