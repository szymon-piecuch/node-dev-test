import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Order } from '../Model/Order';
import { CustomerMapper } from './CustomerMapper';
import { ProductMapper } from './ProductMapper';

@Injectable()
export class OrderMapper {
  @Inject() private repository: Repository;
  @Inject() private customerMapper: CustomerMapper;
  @Inject() private productMapper: ProductMapper;

  async getOrdersForDay(date: Date): Promise<Order[]> {
    const orders = await this.repository.fetchOrders();

    const foundOrders = orders.filter(
      order => new Date(order.createdAt).getTime() === date.getTime(),
    );

    return await Promise.all(foundOrders.map(this.mapDataToModel, this));
  }

  private async mapDataToModel(data: any): Promise<Order> {
    const customer = await this.customerMapper.getCustomerById(data.customer);
    const products = await this.productMapper.getProductsByIds(data.products);

    return new Order(data.number, customer, new Date(data.createdAt), products);
  }
}
