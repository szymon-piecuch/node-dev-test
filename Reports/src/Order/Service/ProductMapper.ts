import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Product } from '../Model/Product';

@Injectable()
export class ProductMapper {
  @Inject() private repository: Repository;

  async getProductsByIds(ids: number[]): Promise<Product[]> {
    const products = await this.repository.fetchProducts();

    const foundProducts = ids.map(id => {
      const foundProduct = products.find(product => product.id === id);

      if (!foundProduct) {
        throw new Error(`Product with id ${id} not found.`);
      }

      return foundProduct;
    });

    return foundProducts.map(this.mapDataToModel, this);
  }

  private mapDataToModel(data: any): Product {
    return new Product(data.id, data.name, data.price);
  }
}
