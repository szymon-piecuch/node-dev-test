import { Test } from '@nestjs/testing';
import { CustomerMapper } from './CustomerMapper';
import { Repository } from './Repository';

describe('CustomerMapper', () => {
  let customerMapper: CustomerMapper;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [Repository, CustomerMapper],
    }).compile();

    customerMapper = module.get<CustomerMapper>(CustomerMapper);
  });

  it('Returns existing customer', async () => {
    expect(await customerMapper.getCustomerById(1)).toEqual({
      firstName: 'John',
      id: 1,
      lastName: 'Doe',
    });
  });

  it('Throws error when customer does not exist', async () => {
    await expect(customerMapper.getCustomerById(5)).rejects.toThrow(
      new Error('Customer with id 5 not found.'),
    );
  });
});
