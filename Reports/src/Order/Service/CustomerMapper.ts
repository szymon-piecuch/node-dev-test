import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Customer } from '../Model/Customer';

@Injectable()
export class CustomerMapper {
  @Inject() private repository: Repository;

  async getCustomerById(id: number): Promise<Customer> {
    const customers = await this.repository.fetchCustomers();
    const foundCustomer = customers.find(customer => customer.id === id);

    if (!foundCustomer) {
      throw new Error(`Customer with id ${id} not found.`);
    }

    return this.mapDataToModel(foundCustomer);
  }

  private mapDataToModel(data: any): Customer {
    return new Customer(data.id, data.firstName, data.lastName);
  }
}
