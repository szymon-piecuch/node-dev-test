import { Test } from '@nestjs/testing';
import { Repository } from './Repository';
import { ProductMapper } from './ProductMapper';

describe('ProductMapper', () => {
  let productMapper: ProductMapper;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [Repository, ProductMapper],
    }).compile();

    productMapper = module.get<ProductMapper>(ProductMapper);
  });

  it('Returns existing products', async () => {
    expect(await productMapper.getProductsByIds([1, 2])).toEqual([
      { id: 1, name: 'Black sport shoes', price: 110 },
      { id: 2, name: 'Cotton t-shirt XL', price: 25.75 },
    ]);
  });

  it('Throws error when one of products does not exist', async () => {
    await expect(productMapper.getProductsByIds([1, 500])).rejects.toThrow(
      new Error('Product with id 500 not found.'),
    );
  });
});
