import { Customer } from './Customer';
import { Product } from './Product';

export class Order {
  constructor(
    public orderNumber: string,
    public customer: Customer,
    public createdAt: Date,
    public products: Product[],
  ) {}

  getTotalPrice(): number {
    return this.products.map(product => product.price).reduce((a, b) => a + b);
  }
}
