export class Customer {
  public constructor(
    public id: number,
    public firstName: string,
    public lastName: string,
  ) {}

  getFullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }
}
