import { Module, Logger } from '@nestjs/common';
import { ReportController } from './Controller/ReportController';
import { OrderModule } from '../Order/OrderModule';
import { BestSeller } from './Service/BestSeller';
import { BestBuyer } from './Service/BestBuyer';

@Module({
  imports: [OrderModule],
  controllers: [ReportController],
  providers: [BestSeller, BestBuyer, Logger],
})
export class ReportModule {}
