import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';

@Injectable()
export class DatePipe implements PipeTransform {
  transform(value: any) {
    const timestamp = Date.parse(value);

    if (isNaN(timestamp)) {
      throw new BadRequestException(`Incorrect date: ${value}`);
    }

    return new Date(timestamp);
  }
}
