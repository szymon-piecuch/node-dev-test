import { Controller, Get, Param, Inject, Logger } from '@nestjs/common';
import { IBestSellers } from '../Model/IReports';
import { BestSeller } from '../Service/BestSeller';
import { BestBuyer } from '../Service/BestBuyer';
import { DatePipe } from '../Pipe/DatePipe';

@Controller()
export class ReportController {
  @Inject() private logger: Logger;
  @Inject() private bestSeller: BestSeller;
  @Inject() private bestBuyer: BestBuyer;

  @Get('/report/products/:date')
  async bestSellers(
    @Param('date', new DatePipe()) date: Date,
  ): Promise<IBestSellers[]> {
    try {
      return await this.bestSeller.getForDay(date);
    } catch (e) {
      this.logger.error(e.message, e);
      throw e;
    }
  }

  @Get('/report/customer/:date')
  async bestBuyers(@Param('date', new DatePipe()) date: Date) {
    try {
      return await this.bestBuyer.getForDay(date);
    } catch (e) {
      this.logger.error(e.message, e);
      throw e;
    }
  }
}
