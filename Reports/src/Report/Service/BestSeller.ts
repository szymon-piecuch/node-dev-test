import { Injectable, Inject } from '@nestjs/common';
import { IBestSellers } from '../Model/IReports';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { Product } from '../../Order/Model/Product';

@Injectable()
export class BestSeller {
  @Inject() private orderMapper: OrderMapper;

  async getForDay(day: Date): Promise<IBestSellers[]> {
    const orders = await this.orderMapper.getOrdersForDay(day);

    const productCounters: { [key: number]: IBestSellers } = orders.reduce(
      (counters, order) => {
        order.products.forEach(product => {
          const existingCounter =
            counters[product.id] || this.createEmptyBestSeller(product);

          counters[product.id] = {
            ...existingCounter,
            quantity: existingCounter.quantity + 1,
            totalPrice: existingCounter.totalPrice + product.price,
          };
        });
        return counters;
      },
      {},
    );

    return Object.values(productCounters).sort(
      (a, b) => b.quantity - a.quantity,
    );
  }

  private createEmptyBestSeller(product: Product): IBestSellers {
    return {
      quantity: 0,
      productName: product.name,
      totalPrice: 0,
    };
  }
}
