import { Injectable, Inject } from '@nestjs/common';
import { IBestBuyers } from '../Model/IReports';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { Customer } from 'src/Order/Model/Customer';

@Injectable()
export class BestBuyer {
  @Inject() private orderMapper: OrderMapper;

  async getForDay(day: Date): Promise<IBestBuyers[]> {
    const orders = await this.orderMapper.getOrdersForDay(day);

    const productCounters: { [key: number]: IBestBuyers } = orders.reduce(
      (counters, order) => {
        const existingCustomer =
          counters[order.customer.id] ||
          this.createEmptyBestBuyer(order.customer);

        counters[order.customer.id] = {
          ...existingCustomer,
          totalPrice: existingCustomer.totalPrice + order.getTotalPrice(),
        };
        return counters;
      },
      {},
    );

    return Object.values(productCounters).sort(
      (a, b) => b.totalPrice - a.totalPrice,
    );
  }

  private createEmptyBestBuyer(customer: Customer): IBestBuyers {
    return {
      customerName: customer.getFullName(),
      totalPrice: 0,
    };
  }
}
